
import requests 


#######requetage openfoodfact########

def isVegan(requestAsJson):
    ingredients = requestAsJson["product"]["ingredients"]
    for ingredient in ingredients:
        try:
            if ingredient['vegan']=='no' or ingredient['vegan']=='maybe':
                return False
        
        except :
            pass

    return True


####webservice#######

from fastapi import FastAPI

app = FastAPI(
    title="IsVegan",
    description="is this product vegan or not ? ",
    version="1.0.0")


@app.get("/")
async def root():
    return {"message": "Is this vegan ? "}


@app.get("/search={query}")
async def output(query):
    requete = requests.get("https://world.openfoodfacts.org/api/v0/product/{}.json".format(query))


    return {"IsVegan":isVegan(requete.json())}

